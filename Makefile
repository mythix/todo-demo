.SILENT:
.PHONY: test build

-include .env
export

ENV ?= dev

ifeq ($(ENV),prod)
CONFIG = docker-compose-prod.yml
else
CONFIG = docker-compose.yml
endif

COMPOSE_CMD = docker-compose -f$(CONFIG)
DOCKER_EXEC = $(COMPOSE_CMD) exec -u dev
DOCKER_PHP_CMD = $(DOCKER_EXEC) php /bin/sh -ec

all: help

# Build docker images
build:
	@echo 'Pull & build required docker images'
	$(COMPOSE_CMD) build

# Start docker containers
start:
	@echo 'Starting containers'
	$(COMPOSE_CMD) up -d

# Stop docker containers
stop:
	@echo 'Stopping containers'
	$(COMPOSE_CMD) down

# ssh into php container
ssh:
	@echo 'SSH to php container'
	$(DOCKER_EXEC) php bash

# Run composer install
install: build start
	@echo 'Installing symfony'
	$(DOCKER_PHP_CMD) 'composer install'
	@echo 'Running migrations'
	$(DOCKER_PHP_CMD) 'bin/console doctrine:migrations:migrate -q'

# Clean symfony cache & logs files
cache-clear:
	@echo 'Remove cache & logs files'
	$(DOCKER_PHP_CMD) 'bin/console cache:clear'

# Run symfony console commands
symfony:
	@echo 'Running symfony console'
	$(DOCKER_PHP_CMD) 'bin/console ${CMD}'

# Build test database and run test suites
load-demo-data:
	@echo 'Loading fixtures for group demo'
	$(DOCKER_PHP_CMD) 'bin/console doctrine:fixtures:load -q --group=demo'

# Build test database and run test suites
test:
	@echo 'Recreating test database'
	$(DOCKER_PHP_CMD) 'bin/console doctrine:database:drop -etest --force --if-exists -q'
	$(DOCKER_PHP_CMD) 'bin/console doctrine:database:create -etest -q'
	$(DOCKER_PHP_CMD) 'bin/console doctrine:schema:create -etest -q'
	@echo 'Loading test fixtures'
	$(DOCKER_PHP_CMD) 'bin/console doctrine:fixtures:load -etest --group=test -q'
	@echo 'Running tests'
	$(DOCKER_PHP_CMD) 'bin/phpunit --testdox --coverage-html public/_coverage'

# Display available make tasks
help:
	@echo ''
	@echo 'Usage: make <recipes>'
	@echo ''
	@echo 'build            Build containers'
	@echo 'start            Start containers'
	@echo 'stop             Stop containers'
	@echo 'ssh              Ssh into the php container'
	@echo 'install          Run composer install in the php container and create database'
	@echo 'cache-clear      Run the symfony cache:clear command'
	@echo 'symfony          Run the symfony console, use CMD="" to pass commands'
	@echo 'load-demo-data   Load fixtures with group "demo"'
	@echo 'test             Setup test database and run test suites'
	@echo ''