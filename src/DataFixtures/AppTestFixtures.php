<?php

namespace App\DataFixtures;

use App\Entity\TodoItem;
use App\Entity\TodoList;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\Persistence\ObjectManager;

class AppTestFixtures extends Fixture implements FixtureGroupInterface
{
    public function load(ObjectManager $manager)
    {
        $manager->persist(new TodoList(
            'list 1',
            [
                'item 1.1',
                'item 1.2',
            ]
        ));
        $manager->persist(new TodoList(
            'list 2',
            [
                'item 2.1',
                'item 2.2',
                'item 2.3',
                'item 2.4',
            ]
        ));

        $manager->flush();
    }

    public static function getGroups(): array
    {
        return ['test'];
    }
}
