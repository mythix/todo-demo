<?php

namespace App\DataFixtures;

use App\Entity\TodoItem;
use App\Entity\TodoList;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\Persistence\ObjectManager;

class AppDemoFixtures extends Fixture implements FixtureGroupInterface
{
    public function load(ObjectManager $manager)
    {
        $manager->persist(new TodoList(
            'Shopping List',
            [
                'Milk',
                'Pancakes',
                'Ice cream',
                'Pizza',
            ]
        ));
        $manager->persist(new TodoList(
            'Chores',
            [
                'Fix leaking roof',
                'Replace locks',
                'Paint cabinets',
            ]
        ));

        $manager->flush();
    }

    public static function getGroups(): array
    {
        return ['demo'];
    }
}
