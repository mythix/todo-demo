<?php

namespace App\Controller;

use App\Form\TodoListType;
use App\Manager\TodoListManager;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;

/**
 * @Route("/todo-list")
 * @Route("/")
 */
class TodoListController
{
    /**
     * @Route("", name="todo_list_index")
     */
    public function indexAction(
        TodoListManager $todoListManager,
        EngineInterface $renderer
    ) {
        return $renderer->renderResponse('todo-list/index.html.twig', [
            'lists' => $todoListManager->getAll(),
        ]);
    }

    /**
     * @Route("/create", methods={"GET", "POST"}, name="todo_list_create")
     */
    public function createNewListAction(
        Request $request,
        FormFactoryInterface $formFactory,
        TodoListManager $todoListManager,
        EngineInterface $renderer,
        RouterInterface $router
    ) {
        $form = $formFactory->create(TodoListType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $todoListManager->upsert($form->getData());
            return new RedirectResponse($router->generate('todo_list_index'));
        }

        return $renderer->renderResponse('todo-list/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}