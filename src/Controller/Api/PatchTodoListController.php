<?php

namespace App\Controller\Api;

use App\Manager\TodoListManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class PatchTodoListController
{
    /**
     * @Route("/todo-list/{id}", methods={"PATCH"})
     */
    public function handle(int $id, Request $request, TodoListManager $manager, SerializerInterface $serializer)
    {
        $list = $manager->getById($id);

        if (!$list) {
            return new Response('List not found', 404);
        }

        if ($request->request->has('title')) {
            $list->setTitle($request->request->get('title'));
            $manager->upsert($list);
        }

        return JsonResponse::fromJsonString($serializer->serialize($list, 'json'));
    }
}