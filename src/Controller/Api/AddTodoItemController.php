<?php

namespace App\Controller\Api;

use App\Entity\TodoItem;
use App\Manager\TodoListManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class AddTodoItemController
{
    /**
     * @Route("/todo-list/{id}/item", methods={"POST"})
     */
    public function handle(int $id, Request $request, TodoListManager $manager, SerializerInterface $serializer)
    {
        $list = $manager->getById($id);

        if (!$list) {
            return new Response('List not found', 404);
        }

        if ($request->request->has('label')) {
            $item = $list->createItem($request->request->get('label'));
            $manager->upsert($list);

            return JsonResponse::fromJsonString($serializer->serialize($item, 'json'));
        }

        return new Response('Required body parameter "label" not found', 406);
    }
}