<?php

namespace App\Controller\Api;

use App\Manager\TodoItemManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MarkTodoItemFinishedController
{
    /**
     * @Route("/todo-item/{id}/finish", methods={"PUT"})
     */
    public function handle(int $id, TodoItemManager $todoItemManager)
    {
        $item = $todoItemManager->getById($id);

        if (!$item) {
            return new Response('TodoItem not found', 404);
        }

        $item->setFinished(true);
        $todoItemManager->upsert($item);

        return new Response(null, 200);
    }
}