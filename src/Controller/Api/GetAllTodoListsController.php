<?php

namespace App\Controller\Api;

use App\Manager\TodoListManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class GetAllTodoListsController
{
    /**
     * @Route("/todo-list", methods={"GET"})
     */
    public function handle(TodoListManager $todoListManager, SerializerInterface $serializer)
    {
        $lists = $todoListManager->getAll();

        return JsonResponse::fromJsonString($serializer->serialize($lists, 'json'));
    }
}