<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

trait DoctrineIdentifiable
{
    /**
     * @var ?int
     * @ORM\Id()
     * @ORM\Column(type="integer", options={"unsigned": true})
     * @ORM\GeneratedValue()
     */
    private $id;

    /**
     * Returns the entity id, or null if it has not been persisted yet
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }
}