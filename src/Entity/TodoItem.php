<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="todo_items")
 */
class TodoItem
{
    use DoctrineIdentifiable;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    private $label;

    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=false)
     */
    private $finished = false;

    /**
     * @var TodoList
     * @ORM\ManyToOne(targetEntity="TodoList", inversedBy="items")
     */
    private $list;

    /**
     * Should not be instantiated directly, managed by root aggregate TodoList
     * @see TodoList::createItem()
     */
    public function __construct(TodoList $list, string $label)
    {
        $this->list = $list;
        $this->label = $label;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function setLabel(string $label): void
    {
        $this->label = $label;
    }

    public function isFinished(): bool
    {
        return $this->finished;
    }

    public function setFinished(bool $finished): void
    {
        $this->finished = $finished;
    }
}