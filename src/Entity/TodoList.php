<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Root Aggregate for Todo Lists
 *
 * @ORM\Entity()
 * @ORM\Table(name="todo_lists")
 */
class TodoList
{
    use DoctrineIdentifiable;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    private $title;

    /**
     * @var TodoItem[]
     * @ORM\OneToMany(targetEntity="TodoItem", mappedBy="list", cascade={"persist"})
     */
    private $items;

    /**
     * @param string $title
     * @param string[] $items
     */
    public function __construct(string $title, array $items = [])
    {
        $this->title = $title;
        $this->items = new ArrayCollection();

        foreach ($items as $item) {
            $this->createItem($item);
        }
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * Returns a reindexed representation of the collection
     * so it can not be tampered with from outside this root aggregate
     *
     * @return TodoItem[]
     */
    public function getItems(): iterable
    {
        return array_values($this->items->toArray());
    }

    public function createItem(string $label): TodoItem
    {
        $item = new TodoItem($this, $label);
        $this->items->add($item);

        return $item;
    }

    public function removeItem(TodoItem $item): void
    {
        $this->items->removeElement($item);
    }
}