<?php

namespace App\Manager;

use App\Entity\TodoList;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityRepository;

class TodoListManager
{
    /** @var ObjectManager */
    private $manager;

    /** @var EntityRepository */
    private $repo;

    public function __construct(ObjectManager $manager, EntityRepository $repo)
    {
        $this->manager = $manager;
        $this->repo = $repo;
    }

    public function upsert(TodoList $list): void
    {
        $this->manager->persist($list);
        $this->manager->flush();
    }

    public function getById(int $id): ?TodoList
    {
        return $this->repo->find($id);
    }

    /**
     * @return TodoList[]
     */
    public function getAll(): iterable
    {
        return $this->repo->findBy([], ['id' => 'desc']);
    }
}