<?php

namespace App\Manager;

use App\Entity\TodoItem;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityRepository;

class TodoItemManager
{
    /** @var ObjectManager */
    private $manager;

    /** @var EntityRepository */
    private $repo;

    public function __construct(ObjectManager $manager, EntityRepository $repo)
    {
        $this->manager = $manager;
        $this->repo = $repo;
    }

    public function upsert(TodoItem $list): void
    {
        $this->manager->persist($list);
        $this->manager->flush();
    }

    public function getById(int $id): ?TodoItem
    {
        return $this->repo->find($id);
    }
}