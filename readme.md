# Todo Demo App

## Technology

- Symfony Flex
- Reactjs
- Docker Compose

## Setup

Application exists of 4 docker containers:

    - php: with xdebug enabled and composer installed globally
    - apache: with php-fpm linked to the php container
    - mysql
    - mysql for test environment

All frontend resources are loaded through CDN and react is running with in-browser transpiling.
There is currently no building or packaging of front end assets.

### Makefile

For platforms supporting make there is a make file with useful recipes available, 
to see a list of recipes use

    make help

### Get the containers ready

#### using Make

    make install

#### using docker-compose
    
    docker-compose build
    docker-compose up -d
    docker-compose exec -udev php /bin/sh -ec 'composer install && bin/console doctrine:migrations:migrate -q'

Add 'todo.local' to your hosts file and the application should be available on http://todo.local:8080/

    echo '127.0.0.1 todo.local' | sudo tee -a /etc/hosts

### Load Demo Data

#### using Make

    make load-demo-data

#### using docker-compose

    docker-compose exec -udev php /bin/sh -ec 'bin/console doctrine:fixtures:load -q --group=demo'

### Run Test Suites

#### using make

    make test

#### using docker-compose

    docker-compose exec -udev php /bin/sh -ec '
      bin/console doctrine:database:drop -etest --force --if-exists -q &&
      bin/console doctrine:database:create -etest -q &&
      bin/console doctrine:schema:create -etest -q &&
      bin/console doctrine:fixtures:load -etest --group=test -q &&
      bin/phpunit --testdox --coverage-html public/_coverage
    '

   
Test coverage will be available on http://todo.local:8080/_coverage/index.html