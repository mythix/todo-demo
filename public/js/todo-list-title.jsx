class TodoListTitle extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            hover: false,
            edit: false
        };
    }

    updateTitle() {
        $.ajax({
            url: `/api/todo-list/${this.props.list.id}`,
            type: 'PATCH',
            data: {
                title: this.props.list.title
            },
            success: () => this.setState({hover: false, edit: false})
        });
    }

    render() {
        if (this.state.edit) {
            return (
                <h5 className="card-title form-inline">
                    <input className="form-control"
                           defaultValue={this.props.list.title}
                           onInput={e => this.props.list.title = e.target.value}
                    />
                    <button className="btn btn-success" onClick={() => this.updateTitle()}>
                        <span className="fa fa-fw fa-check" />
                    </button>
                    <button className="btn btn-danger" onClick={() => this.setState({edit: false, hover: false})}>
                        <span className="fa fa-fw fa-close" />
                    </button>
                </h5>
            );
        }
        if (this.state.hover) {
            return (
                <h5 className="card-title"
                    onClick={() => this.setState({edit: true})}
                    onMouseLeave={() => this.setState({hover: false})}
                ><span className="fa fa-fw fa-edit" onClick={() => this.setState({edit: true})} /> {this.props.list.title}</h5>
            );
        }
        return (
            <h5 className="card-title"
                onMouseEnter={() => this.setState({hover: true})}
            >{this.props.list.title}</h5>
        );
    }
}