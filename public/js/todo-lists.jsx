class TodoLists extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            lists: []
        };

        $.getJSON('/api/todo-list', (result) => this.setState({lists: result}));
    }

    render() {
        return <div className="row">
            {this.state.lists.map((list, index) => <TodoList key={index} list={list} />)}
        </div>;
    }
}