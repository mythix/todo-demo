class TodoListItemAdd extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            form: false
        };
        this.newItemLabel = '';
    }

    saveItem() {
        if (this.newItemLabel.length === 0) {
            return;
        }

        $.post(
            `/api/todo-list/${this.props.list.id}/item`,
            {
                label: this.newItemLabel
            },
            (result) => {
                this.setState({form: false});
                this.newItemLabel = '';
                if (typeof this.props.itemAddHandler == 'function') {
                    this.props.itemAddHandler(result);
                }
            }
        );
    }

    render() {
        if (this.state.form) {
            return <li className="list-group-item list-group-item-light form-inline">
                <input type="text"
                       className="form-control"
                       defaultValue={this.newItemLabel}
                       onInput={e => this.newItemLabel = e.target.value}
                />
                <button className="btn btn-success"
                        onClick={() => this.saveItem()}
                ><span className="fa fa-fw fa-check"/></button>
                <button className="btn btn-danger"
                        onClick={() => this.setState({'form': false})}
                ><span className="fa fa-fw fa-close"/></button>
            </li>;
        }
        return <li className="list-group-item text-center list-group-item-light">
            <button className="btn btn-light"
                    onClick={() => this.setState({'form': true})}
            ><span className="fa fa-fw fa-plus-square text-white" /></button>
        </li>;
    }
}