class TodoListItem extends React.Component {
    constructor(props) {
        super(props);
        this.state =  {
            showCheck: false,
        }
    }

    markFinished() {
        if (typeof this.props.itemFinishedHandler == 'function') {
            $.ajax({
                url: `/api/todo-item/${this.props.item.id}/finish`,
                type: 'PUT',
                success: () => this.props.itemFinishedHandler(this.props.item.id)
            });
        }
    }

    render() {
        if (this.props.item.finished) {
            return <li className="list-group-item list-group-item-light"><del>{this.props.item.label}</del></li>;
        }
        let check = '';
        if (this.state.showCheck) {
            check = <button className="btn btn-sm btn-success py-0 float-right"
                            onClick={() => this.markFinished()}
            >
                <span className="fa fa-fw fa-check" />
            </button>
        }
        return <li className="list-group-item"
                   onMouseEnter={() => this.setState({showCheck: true})}
                   onMouseLeave={() => this.setState({showCheck: false})}>
            {this.props.item.label}
            {check}
        </li>;
    }
}