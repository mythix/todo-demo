class TodoList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            list: this.props.list
        }
    }

    onItemAdded = (item) => {
        this.props.list.items.push(item);
        this.setState({
            list: this.props.list
        });
    };

    onItemFinished = (id) => {
        for (let item of this.props.list.items) {
            if (item.id === id) {
                item.finished = true
            }
        }
        this.setState({
            list: this.props.list
        });
    };

    renderItems(filter) {
        return this.props.list.items
            .filter(filter)
            .map((item, index) => <TodoListItem key={item.id} item={item} itemFinishedHandler={this.onItemFinished} />)
        ;
    }

    render() {
        let items = [].concat(
            this.renderItems((item) => !item.finished),
            this.renderItems((item) => item.finished),
            [
                <TodoListItemAdd key="add"
                                 list={this.props.list}
                                 itemAddHandler={this.onItemAdded}
                />
            ]
        );
        return (
            <div className="col-xl-4 col-lg-6">
                <div className="card mb-3">
                    <div className="card-body">
                        <TodoListTitle list={this.props.list}/>
                    </div>
                    <ul className="list-group list-group-flush">
                        {items}
                    </ul>
                </div>
            </div>
        );
    }
}