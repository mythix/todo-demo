<?php

namespace App\Tests\api;

use App\Entity\TodoItem;
use App\Tests\ObjectManagerHelper;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class MarkTodoItemFinishedControllerTest extends WebTestCase
{
    use ObjectManagerHelper;

    public function testInvalidIdReturns404()
    {
        $client = $this->createClient();
        $client->request('PUT', '/api/todo-item/1050/finish');
        $this->assertSame(404, $client->getResponse()->getStatusCode());
    }

    public function testCanMarkExistingItemFinished()
    {
        // init
        $client = $this->createClient();
        $om = $this->getObjectManager($client);

        // assert starting state
        $item1 = $om->find(TodoItem::class, 1);
        $this->assertFalse($item1->isFinished());

        // call api
        $client->request('PUT', '/api/todo-item/1/finish');
        $this->assertSame(200, $client->getResponse()->getStatusCode());

        // assert changed state
        $item1 = $om->find(TodoItem::class, 1);
        $this->assertTrue($item1->isFinished());
    }
}