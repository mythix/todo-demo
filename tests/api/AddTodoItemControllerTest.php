<?php

namespace App\Tests\api;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AddTodoItemControllerTest extends WebTestCase
{
    public function testInvalidIdReturns404()
    {
        $client = $this->createClient();
        $client->request('POST', '/api/todo-list/1050/item', ['label' => 'my new item']);
        $this->assertSame(404, $client->getResponse()->getStatusCode());
    }

    public function testCanNotAddNewItemWithoutLabel()
    {
        $client = $this->createClient();
        $client->request('POST', '/api/todo-list/1/item', ['title' => 'this should be a label']);
        $this->assertSame(406, $client->getResponse()->getStatusCode());
    }

    public function testCanAddNewItemToExistingList()
    {
        $client = $this->createClient();
        $client->request('POST', '/api/todo-list/1/item', ['label' => 'my new item']);
        $this->assertSame(200, $client->getResponse()->getStatusCode());
        $this->assertJson($client->getResponse()->getContent());
        $data = json_decode($client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('id', $data);
        $this->assertArrayHasKey('label', $data);
        $this->assertSame($data['label'], 'my new item');
    }
}