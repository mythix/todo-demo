<?php

namespace App\Tests\api;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class GetAllTodoListsControllerTest extends WebTestCase
{
    public function testCanGetAllLists()
    {
        $client = $this->createClient();
        $client->request('GET', '/api/todo-list');
        $this->assertSame(200, $client->getResponse()->getStatusCode());
        $this->assertJson($client->getResponse()->getContent());
        $data = json_decode($client->getResponse()->getContent(), true);
        $this->assertCount(2, $data);
    }
}