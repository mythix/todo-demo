<?php

namespace App\Tests\api;

use App\Entity\TodoItem;
use App\Entity\TodoList;
use App\Tests\ObjectManagerHelper;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PatchTodoListControllerTest extends WebTestCase
{
    use ObjectManagerHelper;

    public function testInvalidIdReturns404()
    {
        $client = $this->createClient();
        $client->request('PATCH', '/api/todo-list/1050');
        $this->assertSame(404, $client->getResponse()->getStatusCode());
    }

    public function testCanUpdateListTitle()
    {
        // init
        $client = $this->createClient();
        $om = $this->getObjectManager($client);

        // assert starting state
        $list = $om->find(TodoList::class, 1);
        $this->assertSame('list 1', $list->getTitle());

        // call api
        $client->request('PATCH', '/api/todo-list/1', ['title' => 'new title']);
        $this->assertSame(200, $client->getResponse()->getStatusCode());

        // assert changed state
        $list = $om->find(TodoList::class, 1);
        $this->assertSame('new title', $list->getTitle());
    }
}