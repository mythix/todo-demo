<?php

namespace App\Tests;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;

trait ObjectManagerHelper
{
    /** @var ObjectManager */
    private $objectManager;

    public function getObjectManager(KernelBrowser $client): ObjectManager
    {
        if (!$this->objectManager) {
            $this->objectManager = $client->getContainer()->get('doctrine.orm.default_entity_manager');
        }

        return $this->objectManager;
    }
}