<?php

namespace App\Tests\api;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\RedirectResponse;

class TodoListControllerTest extends WebTestCase
{
    public function testCanRenderHomepageRoute()
    {
        $client = $this->createClient();
        $client->request('GET', '/');
        $this->assertSame(200, $client->getResponse()->getStatusCode());
    }

    public function testCanRenderTodoListRoute()
    {
        $client = $this->createClient();
        $client->request('GET', '/todo-list');
        $this->assertSame(200, $client->getResponse()->getStatusCode());
    }

    public function testCreateFormWithValidDataRedirectsToIndex()
    {
        $client = $this->createClient();
        $client->request('GET', '/todo-list/create');
        $client->submitForm('Create', [
            'todo_list' => [
                'title' => 'my new title',
            ]
        ]);
        $this->assertSame(302, $client->getResponse()->getStatusCode());
        $client->followRedirect();
        $this->assertSame('/todo-list', $client->getRequest()->getPathInfo());
    }

    public function testCreateFormWithInvalidDataRendersErrors()
    {
        $client = $this->createClient();
        $client->request('GET', '/todo-list/create');
        $crawler = $client->submitForm('Create', [
            'todo_list' => [
                'title' => '',
            ]
        ]);
        $this->assertSame(200, $client->getResponse()->getStatusCode());
        $this->assertSame(
            1,
            $crawler->filter('html:contains("This value should not be blank")')->count(),
            'Form error message not found'
        );

    }
}