<?php

namespace App\Tests\unit\Entity;

use App\Entity\TodoItem;
use App\Entity\TodoList;
use PHPUnit\Framework\TestCase;

class TodoItemTest extends TestCase
{
    private $list;

    protected function setUp()
    {
        parent::setUp();

        $this->list = new TodoList('');
    }

    public function testCanNotConstructWithoutListOrLabel()
    {
        $this->expectException(\ArgumentCountError::class);
        new TodoItem();
    }

    public function testCanNotConstructWithoutLabel()
    {
        $this->expectException(\ArgumentCountError::class);
        new TodoItem($this->list);
    }

    public function testCanConstructWithListAndLabel()
    {
        $item = new TodoItem($this->list, 'my item');
        $this->assertSame('my item', $item->getLabel());
        $this->assertSame(false, $item->isFinished());
    }

    public function testCanSetLabel()
    {
        $item = new TodoItem($this->list, 'my item');
        $item->setLabel('new label');
        $this->assertSame('new label', $item->getLabel());
    }

    public function testCanMarkFinished()
    {
        $item = new TodoItem($this->list, 'my item');
        $item->setFinished(true);
        $this->assertSame(true, $item->isFinished());
    }
}