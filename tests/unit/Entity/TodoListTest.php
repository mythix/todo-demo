<?php

namespace App\Tests\unit\Entity;

use App\Entity\TodoItem;
use App\Entity\TodoList;
use PHPUnit\Framework\TestCase;

class TodoListTest extends TestCase
{
    public function testCanNotConstructWithoutTitle()
    {
        $this->expectException(\ArgumentCountError::class);
        new TodoList();
    }

    public function testCanConstructWithTitle()
    {
        $list = new TodoList('my title');
        $this->assertSame('my title', $list->getTitle());
    }

    public function testCanConstructWithTitleAndItems()
    {
        $list = new TodoList('my title', ['first item', 'second item']);

        $this->assertSame('my title', $list->getTitle());
        $this->assertCount(2, $list->getItems());
        $this->assertSame('first item', $list->getItems()[0]->getLabel());
        $this->assertSame('second item', $list->getItems()[1]->getLabel());
    }

    public function testCanUpdateTitle()
    {
        $list = new TodoList('my title');
        $list->setTitle('new title');
        $this->assertSame('new title', $list->getTitle());
    }

    public function testCanCreateNewItems()
    {
        $list = new TodoList('');
        $firstItem = $list->createItem('first item');
        $secondItem = $list->createItem('second item');

        $this->assertCount(2, $list->getItems());
        $this->assertSame('first item', $list->getItems()[0]->getLabel());
        $this->assertSame($firstItem, $list->getItems()[0]);
        $this->assertSame($secondItem, $list->getItems()[1]);
    }

    public function testCanNotManuallyAddItems()
    {
        $list = new TodoList('');
        $list->getItems()[] = new TodoItem($list, 'test');
        $this->assertCount(0, $list->getItems());
    }

    public function testCanRemoveItem()
    {
        $list = new TodoList('');
        $item1 = $list->createItem('item 1');
        $item2 = $list->createItem('item 2');
        $list->removeItem($item1);
        $this->assertCount(1, $list->getItems());
        $this->assertSame('item 2', $list->getItems()[0]->getLabel());
    }
}